# Yêu cầu:
- Nếu sử dụng docker:
	* Dockerfile:
	> - FROM nvidia/cuda:10.1-runtime-ubuntu18.04 
	> - RUN apt update 
	> - RUN apt install -y python3-pip git 
	> - RUN git clone 'https://gitlab.com/truanv/information-retrieval---uitcsgang.git' 
	> - RUN pip3 install numpy h5py scikit-image torch Flask whoosh nltk torchvision
- Nếu không sử dụng docker:
	> - sudo apt install -y python3-pip git
	> - git clone 'https://gitlab.com/truanv/information-retrieval---uitcsgang.git' 
	> - pip3 install numpy h5py scikit-image torch Flask whoosh nltk torchvision
 
# Chạy Hệ Thống Truy Vấn
- Mở terminal -> python3 app.py.
- Mở web browser với url: [http://localhost:5000](http://localhost:5000)
## Insert hình ảnh mới:
- Mở web browser với url: [http://localhost:5000/log?p=a](http://localhost:5000/log?p=a)
- Các bước khi thêm hình ảnh mới của vào hệ thống (đọc thêm): 
1. Các hình ảnh sau khi load lên sẽ được đổi tên thành "Image-{số lượng ảnh hiện tại có trong dataset/img}" số lượng ảnh hiện tại có trong dataset được lưu trong file "current_image_name.json" và được lưu trong thư mục "image_cation"
2. Hệ thống sẽ gọi hàm rút trích caption với các tham số "generate_captions_from_image_folder(img_path='./dataset/image_caption', json_path='./dataset/json/results.json')" Nếu rút trích caption thành công thì trong thu mục dataset/json/ sẽ có file results.json
3. Hệ thống kiểm tra trong thư mục dataset/json xem có tồn tại file results.json hay không: Nếu không có tương đương với rút trích caption không thành công, hệ thống sẽ xóa toàn bộ hình ảnh trong thư mục dataset/image_caption. Ngược lại thì đánh chỉ mục dựa trên file JSON đó, sau khi đánh chỉ mục xong sẽ ghi lại số lượng hình ảnh hiện đang có, chuyển tất cả các hình ảnh trong thư mục dataset/image_cation -> dataset/img và xóa file results.json

# Cấu hình NGINX (Tùy chọn):
:pushpin: Nếu bạn chạy trên localhost thì bỏ qua phần này. :vertical_traffic_light:
:warning: Lưu ý: Cấu hình NGINX trên VPS chứ không phải trên Instance của Docker.
- Cài đặt NGINX:
	> sudo apt install nginx
	- Bạn có thể tham khảo thêm cấu hình cho NGINX [tại đây](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04-quickstart).
- Cấu hình NGINX:
	> - sudo /etc/init.d/nginx start
	> - cd /etc/nginx/sites-enabled/
	> - sudo mv default default.bak
	> - sudo touch ../sites-available/uitcsgang
	> - sudo ln -s  ../sites-available/uitcsgang .
	> - sudo nano ../sites-enabled/uitcsgang
	>>server	{
	>>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; location / {
	>>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;proxy_pass http://<i>127.0.0.1:5000;
	>>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;proxy_set_header Host $host;
	>>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;proxy_set_header X-Real-IP $remote_addr;
	>> &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;}
	>>} 
	>>
	> - sudo /etc/init.d/nginx restart
	- Hướng dẫn chi tiết [tại đây](https://www.youtube.com/watch?v=kDRRtPO0YPA).
