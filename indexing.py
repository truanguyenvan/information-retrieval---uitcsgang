from whoosh.index import open_dir
from JsonExtractor import _JsonExtractor
def create_index_from_file_json(json_path):
    ix = open_dir("./dataset/index")
    writer = ix.writer()
    json_extraction = _JsonExtractor(json_path)
    for file_name in json_extraction.get_filenames():
        title, caption = json_extraction.get_caption_from_file(file_name)
        img_path = file_name
        writer.add_document(title=title, path=img_path, content=caption)
    writer.commit()
