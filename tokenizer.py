from whoosh.analysis import RegexTokenizer, BiWordFilter, StemFilter, StopFilter

def token(doc):
    tokenzier1 = RegexTokenizer() | StemFilter() | StopFilter()
    tokenzier2 = RegexTokenizer() | StemFilter() | StopFilter() | BiWordFilter()
    list = [word.text for word in tokenzier1(doc)]
    if len(list) >= 2:
        for word in tokenzier2(doc):
            list.append(word.text)
    return ' '.join(list)
