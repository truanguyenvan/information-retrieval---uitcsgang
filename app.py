from flask import Flask, render_template, request, send_from_directory, redirect, url_for, flash, session, Session

from searching import searchEngine
from werkzeug.utils import secure_filename
from image_captioning_ruotianluo.genarate_captions import generate_captions_from_image_folder
from indexing import create_index_from_file_json
from InitializerIndex import initiallize_index

import os
import json
import shutil
import re


#khởi tạo thư mục lưu trữ và các hàm xử lý khi ảnh được thêm vào

if not os.path.exists("./dataset/image_caption"):
    os.mkdir("./dataset/image_caption")
if not os.path.exists("./dataset/img"):
    os.mkdir("./dataset/img")
if not os.path.exists("./dataset/index"):
    initiallize_index()


UPLOAD_FOLDER = './dataset/image_caption/'
ALLOWED_EXTENSIONS = set(['jpg'])
app = Flask(__name__)
SESSION_TYPE = 'redis'
app.secret_key = r"A0Zr98j/3yX R~XHH!jmN'LWX/,?RT"

pw = 'a'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def get_current_image_name():
    with open('./dataset/json/current_image_name.json') as infile:
        json_file = json.load(infile)
    return int(json_file['current_image_name'])
def write_current_image_name(count):
    json_file = {"current_image_name": count}
    with open('./dataset/json/current_image_name.json', 'w') as outfile:
        json.dump(json_file, outfile)
def move_image_file_to_dataset():
    path = './dataset/image_caption/'
    for file in os.listdir(path):
        shutil.move(path+file, './dataset/img/' + file)

def remove_file_caption_image_file():
    path = './dataset/image_caption/'
    for file in os.listdir(path):
        os.remove(path + file)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route("/")
def index():
    q = request.args.get("query")
    if q == '' or q is None:
        render_template('index.html')
    return render_template('index.html', value=q)


@app.route("/image/<filename>")
def send_image(filename):
    return send_from_directory("dataset/img", filename)


@app.route("/search", methods=['POST', 'GET'])
def search():
    if request.method == 'POST':
        query = request.form['query']
    else:
        query = request.args.get("query")
    if query == '' or query is None:
        return redirect(url_for('index'))
    else:
        images = searchEngine(query)
        return render_template('search.html', value=query, images=images)

@app.route('/log', methods=['GET'])
def login():
    p = request.args.get("p")
    if p == '' or p is None:
        return redirect(url_for('index'))
    elif p == pw:
        session['p'] = p
        return redirect(url_for('insert'))
    else:
        return redirect(url_for('index'))

@app.route('/insert', methods=['GET', 'POST'])
def insert():

    if 'p' not in session or session['p'] != pw:
        return redirect(url_for('index'))

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        uploaded_files = request.files.getlist("file")
        count = get_current_image_name()
        # if user does not select file, browser also
        # submit a empty part without filename
        for file in uploaded_files:
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            count += 1
            if file and allowed_file(file.filename):
                extention = re.findall(r"\.(\w+)$", secure_filename(file.filename))[0]
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], "image-" + str(count) +'.' + extention))
        generate_captions_from_image_folder(img_path='./dataset/image_caption', json_path='./dataset/json/results.json')
        if os.path.isfile('./dataset/json/results.json') == True:
            create_index_from_file_json('./dataset/json/results.json')
            move_image_file_to_dataset()
            write_current_image_name(count)
            os.remove('./dataset/json/results.json')
        else:
            remove_file_caption_image_file()
        return redirect(url_for('index'))
    return render_template('insert.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

