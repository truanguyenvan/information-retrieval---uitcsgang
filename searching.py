from whoosh.index import open_dir
from whoosh.qparser import QueryParser
from whoosh import qparser
from tokenizer import  token
def search_from_index(query):
    list_of_img_paths = list()
    ix = open_dir("./dataset/index")
    query = token(query)
    with ix.searcher() as searcher:
        query = QueryParser("content", ix.schema, group=qparser.OrGroup).parse(query)
        results = searcher.search(query, limit=None)
        for i in results:
            list_of_img_paths.append((i['path'], i['title']))
    return list_of_img_paths

def searchEngine(query=""):
    list_of_img_paths = search_from_index(query)
    return list_of_img_paths
