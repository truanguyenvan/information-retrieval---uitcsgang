import  json
from tokenizer import token
class _JsonExtractor():
    def __init__(self, path):
        self.name_to_index = dict()
        self.index_to_name = dict()
        self.filenames = []
        with open(path) as f:
            self.json_file = json.load(f)

        # Create hash table for mapping from file to index and get filenames
        for idx, result in enumerate(self.json_file):
            self.name_to_index[result['file_name'].split('/')[3]] = idx
            self.index_to_name[idx] = result['file_name'].split('/')[3]
            self.filenames.append(result['file_name'].split('/')[3])

    def get_filenames(self):
        return self.filenames

    def get_name_to_index(self):
        return self.name_to_index

    def get_index_to_name(self):
        return self.index_to_name

    def get_caption_from_file(self, img_name):
        '''return a string list of captions of given json file'''
        caption  = self.json_file[self.name_to_index[img_name]]['caption']
        return caption, token(caption)
#b = _JsonExtractor('./dataset/json/results.json')
#print(b.get_filenames())