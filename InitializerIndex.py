import os
from whoosh.index import create_in
from whoosh.fields import *
from image_captioning_ruotianluo.genarate_captions import generate_captions_from_image_folder
from indexing import create_index_from_file_json

def initiallize_index():
    schema = Schema(title=TEXT(stored=True), path=ID(stored=True), content=TEXT)
    if not os.path.exists("dataset/index"):
        os.mkdir("dataset/index")
    create_in("dataset/index", schema)

#generate_captions_from_image_folder(img_path='./dataset/img', json_path='./dataset/json/results.json')
#initiallize_index()
#create_index_from_file_json('./dataset/json/results.json')
#os.remove('./dataset/json/results.json')